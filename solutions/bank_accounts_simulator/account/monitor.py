from functools import wraps


def monitor(f):
    monitor.thresholds = {}
    @wraps(f)
    def __wrapper_function(*args, **kwargs):
        value = args[2]
        owner = args[0].owner
        if owner in monitor.thresholds:
            threshold = monitor.thresholds[owner]
            if value > threshold:
                print(f"It is the 1st time that {owner} makes a transfer above ${value}")
                monitor.thresholds[owner] = value
        else:
            monitor.thresholds[owner] = value
        return f(*args, **kwargs)
    return __wrapper_function

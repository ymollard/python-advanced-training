"""
Implementation of property-based test cases using hypothesis
"""

from datetime import datetime
from account.internal import BankAccount
from hypothesis import given, strategies as st  # pip install hypothesis

@given(st.integers(min_value=1, max_value=100), st.datetimes())
def test_transfer_to_property_based(amount, date):
    a = BankAccount("A", 100)
    b = BankAccount("B", 100)
    a.transfer_to(b, amount, date)
    assert a.balance == 100 - amount and b.balance == 100 + amount

@given(st.integers(min_value=1, max_value=100), st.datetimes())
def test_credit_property_based(amount, date):
    a = BankAccount("A", 100)
    a._credit(amount, date)
    assert a.balance == 100 + amount


from account.external.blocked import BlockedBankAccount, InsufficientBalance
from datetime import datetime
import pytest


def test_blocked_transfer():
    a = BlockedBankAccount("A", 100)
    b = BlockedBankAccount("B", 100)
    with pytest.raises(InsufficientBalance):
        a.transfer_to(b, 200, datetime(2024, 1, 1))


def test_not_blocked_transfer():
    a = BlockedBankAccount("A", 100)
    b = BlockedBankAccount("B", 100)
    a.transfer_to(b, 10, datetime(2024, 1, 1))
    assert a.balance == 90 and b.balance == 110

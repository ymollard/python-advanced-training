from datetime import datetime
from account.internal import BankAccount


def test_transfer_to():
    a = BankAccount("A", 100)
    b = BankAccount("B", 100)
    a.transfer_to(b, 10, datetime(2024, 1, 1))
    assert a.balance == 90 and b.balance == 110


def test_credit():
    a = BankAccount("A", 100)
    a._credit(10, datetime(2024, 1, 1))
    assert a.balance == 110

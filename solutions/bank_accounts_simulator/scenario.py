#!/usr/bin/env python

from datetime import datetime
from account.external.agios import AgiosBankAccount
from account.internal import BankAccount


if __name__ == "__main__":
    bank = BankAccount("HSBC", 10000)
    walmart = BankAccount("Walmart", 5000)
    alice = AgiosBankAccount("Alice Worz", 500, bank)
    bob = AgiosBankAccount("Bob Müller", 100, bank)

    # 1.4.1. Alice buys $100 of goods at Walmart
    alice.transfer_to(walmart, 100, datetime(2024, 11, 1))
    # 1.4.2. Bob buys $100 of goods at Walmart
    bob.transfer_to(walmart, 100, datetime(2024, 11, 3))
    # 1.4.3. Alice makes a donation of $100 to Bob
    alice.transfer_to(bob, 100, datetime(2024, 11, 4))
    # 1.4.4. Bob buys $200 at Walmart
    bob.transfer_to(walmart, 200, datetime(2024, 11, 10))
    # 3.3. Alice makes a transfer to Bob 5 days later
    alice.transfer_to(bob, 150, datetime(2024, 11, 15))

    for account in [bank, walmart, alice, bob]:
        print(account)

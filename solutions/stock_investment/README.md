# Solutions of the exercise about Stock Investment
## Create a venv

```bash
python -m venv myenv
source myenv/bin/activate
pip install -r requirements.txt
```

## Get data file

```
wget https://gitlab.com/ymollard/python-advanced-training/-/raw/main/exercises/data/stocks.json .
```

## Run one of the different versions of the script

- `get_best_porfolio.py` gets the best portfolio with a single process. Read `stock_investment_summary.md` for its time complexity
- `get_best_porfolio_ncpus.py` runs the above script in multiprocessing
- `get_best_porfolio_njit.py` runs the script optimized with numba. Hwoever, systematic conversion of data to numpy arrays make it not that efficient

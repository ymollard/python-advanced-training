# Complexity of the stock investment mini-project

**Time complexity of combinations**: `n = 26` possible stocks -> `O(2**26 - 1)`

Demo for `n = 4` available stocks:
```python
In [1]: len(list(combinations("ABCD", 4))) +
        len(list(combinations("ABCD", 3))) +
        len(list(combinations("ABCD", 2))) +
        len(list(combinations("ABCD", 1)))
Out[1]: 15

In [2]: 2**4-1
Out[2]: 15
```

**Time complexity of sums per combination**: Each combination sums all stock costs and profits once -> `O(n)` per combination

**Total time complexity**: `O(n.(2**n - 1))`

**Total space complexity**: 
- Without multiprocessing: `O(n)` since we only store stock data once each 
- With multiprocessing: `O(2**n - 1)` since we generate all combinations into memory

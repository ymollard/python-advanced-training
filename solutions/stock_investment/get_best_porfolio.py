"""
Single-process basic version
"""

from json import load
import os, itertools
from typing import TypedDict, Iterable, Optional


AvailableStocks = dict[str, dict[str, int]]
"""
Stocks available for purchase are described in the original JSON data file
"""

class StockPortfolio(TypedDict):
    """
    A portfolio is a combination of **several** stocks with a total cost and total profit
    """
    profit: int
    cost: int
    stock_names: Iterable[str]


def get_best_portfolio_for_budget(portfolios: Iterable[Iterable[str]], stocks: AvailableStocks, budget:int = 500) -> StockPortfolio:
    """
    Get the most profitable sock portfolio among all input portfolios that can be bought with the limited budget
    The stock costs are read from the input `stocks` read from the JSON data file
    :param portfolios: collection of portfolios of one or several stock name(s) each 
    :param stocks: dictionary describing the individual stock costs and profits in percentage 
    :returns: dictionary describing the more profitable portfolio given the limited budget
    """
    best: StockPortfolio = {'profit': 0, 'cost': 0, 'stock_names': ()}
    for portfolio in portfolios: 
        cost = sum(stocks[stock]["cost"] for stock in portfolio)      
        if cost <= budget:
            profit = sum(stocks[stock]["cost"] * stocks[stock]["profit"] for stock in portfolio)
            current: StockPortfolio = {'profit': profit, 'cost': cost, 'stock_names': portfolio}
            best = get_best_portfolio([current, best])
    return best


def get_best_portfolio(portfolios: Iterable[StockPortfolio]) -> StockPortfolio:
    """
    Get the most profitable sock portfolio among all input portfolios
    If two portfolios have the same profit, returns the less expensive one
    :param portfolios: collection of dictionaries representing portfolios of one or several stock(s) each 
    :returns: the more profitable or less expensive portfolio from the provided input
    """
    best: StockPortfolio = {'profit': 0, 'cost': 0, 'stock_names': ()}
    for portfolio in portfolios:
        if portfolio['profit'] > best['profit'] or \
            (portfolio['profit'] == best['profit'] and portfolio['cost'] < best['cost']):
            best = portfolio
    return best


if __name__ == '__main__':
    with open("stocks.json") as f:
        stocks: AvailableStocks = load(f)  # Sample content: stocks["A"] = {"cost":  20, "profit":  5}

    def combination_generator(stocks):
        keys = stocks.keys()
        for r in range(1, len(keys) + 1):
            for combination in itertools.combinations(keys, r):
                yield combination

    best = get_best_portfolio_for_budget(combination_generator(stocks), stocks, budget = 500)

    print(f"Best is earning {best['profit']} for {best['cost']} spent with stocks: {best['stock_names']}")

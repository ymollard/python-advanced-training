from json import load
import os, itertools
from typing import TypedDict, Iterable, Optional
from multiprocessing import Pool, Manager
from multiprocessing.queues import Queue

NUM_CPU = 4  # Number of processes for later multiprocessing

AvailableStocks = dict[str, dict[str, int]]
"""
Stocks available for purchase are described in the original JSON data file
"""

class StockPortfolio(TypedDict):
    """
    A portfolio is a combination of **several** stocks with a total cost and total profit
    """
    profit: int
    cost: int
    stock_names: Iterable[str]


def work(queue: Queue[Optional[Iterable[Iterable[str]]]], stocks: AvailableStocks) -> StockPortfolio:
    """
    Function to be run by a worker process
    It gets iterables of porfolios from the queue and memorizes the best portfolio among all computation tasks
    When the queue returns the final sentinel (None), the worker cleanly closes itself and return the best portfolio
    :param queue: queue storing iterables of portfolios to be processed by workers
    :param stocks: dictionary describing the individual stock costs and profits in percentage 
    :returns: dictionary describing the more profitable portfolio among all portfolios got from the queue
    """
    print(f"Starting worker {os.getpid()}...")
    best: StockPortfolio = {'profit': 0, 'cost': 0, 'stock_names': ()}
    raise NotImplementedError("Implement work() according to its docstring")
    print(f"Worker {os.getpid()} ended!")
    return best


def get_best_portfolio_for_budget(portfolios: Iterable[Iterable[str]], stocks: AvailableStocks, budget:int = 500) -> StockPortfolio:
    """
    Get the most profitable sock portfolio among all input portfolios that can be bought with the limited budget
    The stock costs are read from the input `stocks` read from the JSON data file
    :param portfolios: collection of portfolios of one or several stock name(s) each 
    :param stocks: dictionary describing the individual stock costs and profits in percentage 
    :returns: dictionary describing the more profitable portfolio given the limited budget
    """
    best: StockPortfolio = {'profit': 0, 'cost': 0, 'stock_names': ()}
    raise NotImplementedError("Implement get_best_portfolio_for_budget() according to its docstring")
    return best


def get_best_portfolio(portfolios: Iterable[StockPortfolio]) -> StockPortfolio:
    """
    Get the most profitable sock portfolio among all input portfolios
    If two portfolios have the same profit, returns the less expensive one
    :param portfolios: collection of dictionaries representing portfolios of one or several stock(s) each 
    :returns: the more profitable or less expensive portfolio from the provided input
    """
    best: StockPortfolio = {'profit': 0, 'cost': 0, 'stock_names': ()}
    raise NotImplementedError("Implement get_best_portfolio according to its docstring")
    return best


if __name__ == '__main__':
    with open("stocks.json") as f:
        stocks: AvailableStocks = load(f)  # Sample content: stocks["A"] = {"cost":  20, "profit":  5}

    raise NotImplementedError("Implement __main__ according to questions")

    best = get_best_portfolio(results)
    print(f"Best is earning {best['profit']} for {best['cost']} spent with stocks: {best['stock_names']}")
